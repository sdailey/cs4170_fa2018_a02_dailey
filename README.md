### Assignment 2 ###
This is the repo for Assignment 2

## Summary ##
This week we worked on reimplementing the *Particle Swarm Optimization* algorithm in *OpenMP* instead of *pThreads*. This was achieved by using a combination of the *OpenMP* library, the *Mersenne Twister* function (originally, later changed to a threadsafe function) and the *Rastrigin function*.  
  
### A quick breakdown of each of these terms/concepts ###  
**OpenMP:** OpenMP, also known as Open Multi-Processing is an API used to easily parallelize code.
  
**Particle Swarm Optimization:** Particle swarm optimization is a computational method that aims to solve a problem by creating a set of "particles". Each of these particles move relative to the best local position based on their fitness. As more particles find better positions that are more accurate more particles "swarm" to their location, and ultimately should find the best solution.
  
**Mersenne twister:** The Mersenne Twister is a pseudorandom number generator. It is the most common PRNG and is known to be the closest to random as possible. The name originates from the Mersenne prime.

**Rastrigin function:** The Rastrigin function is a non-convex function that is generally used to test parallelization and optimization algorithms. It is used in this program to create a sample set for the PSO function to work with.

## Instructions ##
_In OSC:_

1. First, ensure that any previously compiled code is removed and then execute the makefile
```
make clean
make all
```

2. Next you need to submit the job 
```
qsub submitJob
```

3. Once you have ensured that the job has completed you can check for your output using the `ls` command

4. Finally, execute the program
```
./OpenMP
```
  
## Algorithm ##
Particle swarm optimization is a computational method that aims to solve a problem by creating a set of "particles". Each of these particles move relative to the best local position based on their fitness. As more particles find better positions that are more accurate more particles "swarm" to their location, and ultimately should find the best solution.

## Method ##
In this project the three update loops were parallelized to ensure proper parallelization between the three main functions of the program. Position, fitness and velocity were all update through a combination of `#pragma omp parallel` commands and `#pragma omp sections`. The first being used by position and velocity, and the second being used for fitness. Position and velocity were split into their own tasks because they only update using one distinct outer `for` loop. This allowed us to simply split all the threads at this loop point. However, with the fitness function, it required additional work to ensure that both sections were parallelized properly. The fitness function has two main loops that update the fitness information. In order to properly divide these two loops and ensure proper synchronization, I split the two for loops with a `#pragma omp sections` directive, then surrounded the two interior loops with `#pragma omp section` so they have proper barriers and to ensure that the second loop does not begin until all threads have exited the first loop. For scheduling in this algorithm I experimented with `guided`, `dynamic`, `static` and even tried `auto` to allow the compiler to take control of the scheduling, however I observed no noticeable changes in runtime and left them out. Additionally, the velocity section has two `private` variables, `R1` and `R2`. This was done to keep the variables unique between all of the threads to ensure that the data is not improperly manipulated by multiple threads at the same time.

## Discussion and Analysis ##
In the results folder there are three files. One is the csv output of the parallel code, another is a csv from the serial code and the last is a combination of the two in the form of an excel sheet. In the excel sheet you will find a thorough analysis of speedup and efficiency for each trial. The results of this analysis have demonstrated that my code produced a slow down and did not execute efficiently. There is also a noticeable increase in slowdown after the 6th or 7th trial, where the speedup plummets to around .625 and never recovers.